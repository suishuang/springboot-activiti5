# springboot-activiti5

#### 介绍
这是spring boot框架集成activiti5工作流实现的SDK，采用目前流行的restful api接口调用.非常方便嵌入业务系统中

#### 软件架构
 **接口文档地址** ：http://qxsdcloud.com/wiki/share/iwINC5KjL


演示地址：http://qxsdcloud.com:18081/   用户名/密码：activiti/activiti

使用手册：https://blog.csdn.net/yabushandaxue/article/details/116134595


基础教程：
[activiti专栏](https://blog.csdn.net/yabushandaxue/category_11241856.html)


使用过程如有疑问可加Q咨询：392425349



本册文档已完善：

1、工作流API



2、API服务调用接口


# **工作流使用注意事项：**

1、流程图采用activiti提供的设计器进行设计；

2、流程图每个任务节点需要设置创建时的监听器，服务类为：*com.yabushan.activiti.tasklistener.GroupTaskListener*

3、根据接口文档中每个接口的入参情况传入参数即可；

4、流程图可自由设计，所有接口支持动态传参。



部分项目截图：


![输入图片说明](https://img-blog.csdnimg.cn/202108060054475.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3N4ZjE5OTc=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")



![输入图片说明](https://img-blog.csdnimg.cn/20210806005456899.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3N4ZjE5OTc=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")



![输入图片说明](https://img-blog.csdnimg.cn/20210806005503266.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3N4ZjE5OTc=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")



![输入图片说明](https://img-blog.csdnimg.cn/2021080600551249.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3N4ZjE5OTc=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")

![输入图片说明](https://img-blog.csdnimg.cn/20210806005519873.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3N4ZjE5OTc=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")


![输入图片说明](https://img-blog.csdnimg.cn/20210806005528326.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3N4ZjE5OTc=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")


![输入图片说明](https://img-blog.csdnimg.cn/20210806005534633.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3N4ZjE5OTc=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")